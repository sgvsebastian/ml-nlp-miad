# Taller: Construcción e implementación de árboles de decisión y métodos de ensamblaje
## Overview
Para realizar este taller, debió haber clonado antes el repositorio del curso en su github. Luego, abra el siguiente Jupyter Notebook y siga las instrucciones que en él se encuentran. Para realizar la entrega, deberá adjuntar el enlace de su repositorio en Git, que contenga el Notebook con la solución de cada uno de los puntos.
## Review Criteria
1. Escribe las inferencias sobre el comportamiento de las dos variables (Hour y workingday). Hace uso de estadística descriptiva como por ejemplo máximo, mínimo y percentiles al hacer las inferencias. [5 puntos]

2. Grafica de manera independiente la renta promedio cuando la variable “workingday” tiene un valor de 0 y cuando tiene un valor de 1. Además, escribe los hallazgos de cada una de las gráficas. [5 puntos]

3. Ajusta un modelo de regresión lineal, considerando las variables "hour" y "workingday" e interpreta los coeficientes de la regresión. Además, describe las limitaciones del modelo considerando las particularidades del problema. [5 puntos] 

4. El algoritmo utilizado sigue la lógica correcta para la creación manual de un árbol de decisión. El resultado obtenido itera sobre las variables "hour" y "workingday" y tiene al menos seis nodos. [5 puntos] 

5. Hace uso de la librería de sklearn para implementar un árbol de decisión; considera las variables "hour" y "workingday" y realiza la calibración de los parámetros del modelo. Además, usa alguna métrica de desempeño de modelos de clasificación para analizar el compartimiento del árbol obtenido en comparación al desempeño del modelo del punto 3. [5 puntos]

6. Construye una regresión logística y un árbol de decisión usando correctamente la librería de sklearn. El árbol lo calibra con al menos un parámetro y compara satisfactoriamente los desempeños de los modelos con el Accuracy y el F1-Score. [5 puntos]

7. Construye correctamente un modelo de ensamble con la metodología de votación mayoritaria con 300 muestras bagged para tres escenarios: 100 árboles de decisión donde el parámetro max_depth = None, 100 árboles de decisión donde el parámetro max_depth = 2 y 100 regresiones logísticas. Además, compara el desempeño de los modelos con el Accuracy y F1-score. [5 puntos]

8. Construye correctamente un modelo de ensamble con la metodología de votación ponderada con 300 muestras bagged para tres escenarios: 100 árboles de decisión donde el parámetro max_depth = None, 100 árboles de decisión donde el parámetro max_depth = 2 y 100 regresiones logísticas. Además, compara el desempeño de los modelos con el Accuracy y F1-score. [5 puntos]

9. Compara los resultados de desempeño obtenidos con las dos metodologías implementadas en los puntos 7 y 8, y enuncia las ventajas o desventajas de cada metodología. [5 puntos]