SHELL=/bin/sh

.PHONY: run

all: help

run: ## Run docker compose
	@docker-compose up --build

help:
	@printf "Available targets:\n\n"
	@printf "Usage: make [target] ...\n"
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-30s\033[0m %s\n", $$1, $$2}'

	