FROM jupyter/r-notebook:ubuntu-20.04

COPY --chown=${NB_UID}:${NB_GID} requirements.txt /tmp/

RUN conda install -y rpy2 && \
    pip install --no-cache-dir --requirement /tmp/requirements.txt && \
    fix-permissions "${CONDA_DIR}" && \
    fix-permissions "/home/${NB_USER}" && \
    jupyter labextension install @jupyter-widgets/jupyterlab-manager jupyterlab-plotly  && \
    rm -rf work
ENTRYPOINT ["jupyter", "lab", "--ip=0.0.0.0", "--allow-root"]
